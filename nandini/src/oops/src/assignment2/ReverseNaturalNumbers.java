package assignment2;

import java.util.Scanner;

public class ReverseNaturalNumbers {

	public static void main(String[] args) {
		Scanner s1= new Scanner(System.in);
		System.out.println("Enter The Value Of N "); 
		int n=s1.nextInt();
		int i=1;
		System.out.println("Natural Numbers");
		while(i<=n) {
			System.out.println(i); 
			i--;
		}

	}

}

